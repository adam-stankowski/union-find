**Quick Find**

This is an eager implementation of a Dynamic Connectivity problem which 
helps discover whether two items are connected with each other. 

{0} {1,3,5} {2,4,8,9}

in the above example 1 - 3 - 5 are connected and the question is whether
3 and 8 are connected. Clearly not. The key here is to implement an algorithm which 
helps provide an answer for any pair. 

This particular implementation is an eager implementation which requires 
constant number of array accesses for isConnected but N array accesses for 
connect operation, which is rather slow. There are algorightm that are much quicker.

**Algotithm**
Initially we start with an array where each entry is equal to its index
`[0 1 2 3 4 5] - indexes
[0 1 2 3 4 5] - values`
now when we connect 4 and 3 we change the value of the first item into value of the second one
`[0 1 2 3 4 5]
[0 1 2 3 3 5]`
this means 3 and 4 are connected
if we connect 3 and 1 then all of the items that have a value 3 will need to change to 1
`[0 1 2 3 4 5]
[0 1 2 1 1 5]`
and so on