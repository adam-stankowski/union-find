package as.quickfind;

public interface QuickFind {
    void connect(int a, int b);
    boolean isConnected(int a, int b);
}
