package as.quickfind;

import java.util.stream.IntStream;

public class QuickFindEager implements QuickFind {
    private int[] elements;

    public QuickFindEager(int numberOfItems) {
        elements = IntStream.range(0, numberOfItems).toArray();
    }

    @Override
    public void connect(int a, int b) {
        if (isConnected(a, b)) {
            return;
        }

        int valueOfA = elements[a];
        int valueOfB = elements[b];

        elements[a] = valueOfB;
        makeAllEntriesWithValueAHaveValueB(valueOfA, valueOfB);
    }

    @Override
    public boolean isConnected(int a, int b) {
        return elements[a] == elements[b];
    }

    private void makeAllEntriesWithValueAHaveValueB(int valueOfA, int valueOfB) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == valueOfA) {
                elements[i] = valueOfB;
            }
        }
    }
}
