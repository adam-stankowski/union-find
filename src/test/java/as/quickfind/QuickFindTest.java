package as.quickfind;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class QuickFindTest {
    private QuickFind quickFind;

    @Before
    public void setUp() {
    }

    @Test
    public void whenNoConnectsThenIsConnectedIsFalse() {
        quickFind = new QuickFindEager(2);
        boolean connected = quickFind.isConnected(0, 1);
        assertThat(connected, is(false));
    }

    @Test
    public void whenTwoItemsConnectedThenTrue() {
        quickFind = new QuickFindEager(2);
        quickFind.connect(0, 1);
        boolean connected = quickFind.isConnected(0, 1);
        assertThat(connected, is(true));
    }

    @Test
    public void furtherTests() {
        quickFind = new QuickFindEager(6);
        quickFind.connect(3, 4);
        assertThat(quickFind.isConnected(3, 4), is(true));
        quickFind.connect(3, 1);
        assertThat(quickFind.isConnected(4, 1), is(true));
        quickFind.connect(5, 4);
        assertThat(quickFind.isConnected(5, 1), is(true));
        assertThat(quickFind.isConnected(5, 3), is(true));
        assertThat(quickFind.isConnected(5, 4), is(true));
    }

    @Test
    public void testsWithTenElementArray() {
        quickFind = new QuickFindEager(10);
        quickFind.connect(3, 4);
        assertThat(quickFind.isConnected(3, 4), is(true));
        quickFind.connect(5, 6);
        assertThat(quickFind.isConnected(3, 5), is(false));
        quickFind.connect(3, 8);
        assertThat(quickFind.isConnected(4, 8), is(true));
        assertThat(quickFind.isConnected(6, 8), is(false));
        quickFind.connect(9, 4);
        assertThat(quickFind.isConnected(8, 9), is(true));
        assertThat(quickFind.isConnected(3, 9), is(true));
        assertThat(quickFind.isConnected(4, 9), is(true));
        assertThat(quickFind.isConnected(2, 9), is(false));
        quickFind.connect(2, 1);
        quickFind.connect(8, 9);
        assertThat(quickFind.isConnected(5, 0), is(false));
        quickFind.connect(5, 0);
        assertThat(quickFind.isConnected(6, 0), is(true));
        quickFind.connect(7, 2);
        quickFind.connect(6, 1);
        assertThat(quickFind.isConnected(7, 0), is(true));
        assertThat(quickFind.isConnected(7, 8), is(false));

    }

}